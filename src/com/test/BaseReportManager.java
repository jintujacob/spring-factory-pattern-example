

package com.test;

import java.util.List;


public abstract class BaseReportManager implements IReportManager
{
	
	public <T extends BaseReportBean> void searchReport(T searchCriteria, List<T> searchResults) 
			throws Exception {
		// to be implemented by the concrete manager classes
	}

	
	public <T extends BaseReportBean> void requestReport(T requestCriteria)
			throws Exception {
		// to be implemented by the concreate manager classes.
	}
	
	

}
