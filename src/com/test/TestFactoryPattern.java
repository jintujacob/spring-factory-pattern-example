package com.test;


public class TestFactoryPattern {

	public static void main(String[] args) {
		ReportManagerFactory factory =  new ReportManagerFactory();
		IReportManager capManager = factory.getManagerInstance("roleType");
		
		try {
			capManager.requestReport(new ChildReportBean());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
