package com.test;

import java.util.Arrays;



public class ReportManagerFactory {
	
	private IReportManager childReportManager;
	private IReportManager anotherChildReportManager;
	
	private static final String[] X_Roles = {"116_01", "116_02", "116_03", "116_04"};
	private static final String[] Y_Roles = {"undefined1", "undefined2" };
	
	
	
	public void setCapReportsManager(ChildReportsManager capReportsManager) {
		this.childReportManager = capReportsManager;
	}
	
	
	/**
	 * getManagerInstance() returns the Concrete implementation of the manager class
	 * depending upon the fileType/roleType recieved as parameter
	 * 
	 * @param fileType
	 * @return IReportManager
	 * @throws UnsupportedOperationException
	 */
	public IReportManager getManagerInstance(String fileType) throws UnsupportedOperationException{
		//filetype variable contains roleid is same as the role id in the programroles table.
		
		
		if(Arrays.asList(X_Roles).contains(fileType))
		{
			return childReportManager;
		}
		else if(Arrays.asList(Y_Roles).contains(fileType))
		{
			return anotherChildReportManager;
		}
		else{
			throw new UnsupportedOperationException();
		}
	}
	
	
	

}
