

package com.test;

import java.util.List;

public interface IReportManager {
	
	public <T extends BaseReportBean> void searchReport( T searchCriteria, List<T> searchResults) 
			throws Exception;
	
	public <T extends BaseReportBean> void requestReport(T requestCriteria)
			throws Exception; 
	 
}
